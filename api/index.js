const express = require('express');
const app = express();
const server = require('http').createServer(app);
const cors = require('cors');
const bodyParser = require('body-parser');
const fs = require('fs');
const io = require('socket.io')(server);

const portIo = 8081;
io.listen(portIo, {
  pingTimeout: 10000,
  pingInterval: 5000,
});

const port = 8080;
server.listen(port);
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

const transcript = []

io.on('connect', (socket) => {
  console.log('client connected');
  io.emit('socketConnected')
})


app.post('/process', (req, res) => {
const sentences = req.body.text.split(/(\?|\.|!|:|,)/);
test = sentences.map((element, i) => sentences[i + 1] ? [sentences[i], sentences[i + 1]] : [sentences[i]]);
console.log(sentences)
// Don't forget to install sox on your computer (brew install sox)
const recorder = require('node-record-lpcm16');
// Imports the Google Cloud client library
const speech = require('@google-cloud/speech');
// Creates a client
const client = new speech.SpeechClient();
/**
 * TODO(developer): Uncomment the following lines before running the sample.
 */
const encoding = 'LINEAR16';
const sampleRateHertz = 16000;
const languageCode = 'fr-FR';
const request = {
  config: {
    encoding: encoding,
    sampleRateHertz: sampleRateHertz,
    languageCode: languageCode,
    speechContexts: [{
        //"phrases": sentences,
    }]
  },
  interimResults: true, // If you want interim results, set this to true
};
// Create a recognize stream
const recognizeStream = client
  .streamingRecognize(request)
  .on('error', console.error)
  .on('data', data => {
  process.stdout.write(
    data.results[0] && data.results[0].alternatives[0]
      ? `Transcription: ${data.results[0].alternatives[0].transcript}\n`
      : `\n\nReached transcription time limit, press Ctrl+C\n`
  )
  data.results.forEach(element => {
    const text = element.alternatives[0].transcript;
    console.log(text);
    io.emit('newWord', text);
  });
  
  }
  );
// Start recording and send the microphone input to the Speech API
recorder
  .record({
    sampleRateHertz: sampleRateHertz,
    threshold: 0,
    // Other options, see https://www.npmjs.com/package/node-record-lpcm16#options
    verbose: false,
    recordProgram: 'sox', // Try also "arecord" or "sox"
    silence: '10.0',
  })
  .stream()
  .on('error', console.error)
  .pipe(recognizeStream);
console.log('Listening, press Ctrl+C to stop.');
});