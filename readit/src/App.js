import React, { useState }from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';

import io from 'socket.io-client';
const  socket = io('http://localhost:8081');

const text = "Tout d'abord, l’écriture web n’est pas qu’une pratique rédactionnelle. Écrire pour le média web, c’est avant tout chercher à maximiser les retours sur investissement des dispositifs éditoriaux : retour sur attention, sur effort et sur contenu. Plus précisément, on n’écrit pas pour le web, mais pour des lecteurs curieux, des utilisateurs avertis, des prospects et clients exigeants. Le rôle de l’écriture web est donc de répondre efficacement à leurs besoins avec une stratégie de contenu. Il s’agit de penser et produire des contenus service, à haut potentiel : attrayants, utiles, performants, pérennes";
const array = text.replace(/(,|;|\.|!|\?|:)/gm, '').split(" ");
    
let current = 0;

class App extends React.Component {
  // const [text, setText] = useState("")
  // const [answer, setAnswer] = useState(false)

  // return (
  //   <div className="App">
  //     <textarea id="texteArea" onChange={(e) => setText(e.target.value)}>
  //     </textarea>
      

  constructor(props) {
    super(props);
    this.state = {
      style: array.map(()=>0)
    }
    this.state.style[0] = 1;
    socket.on('newWord', this.computeNewWord);

  }
  computeNewWord = (str) => {
    const words = str.split(" ");
    words.forEach( (word) => {
      console.log(array[current].replace(/(\,|\;|\.|\!|\?|\'|\’|\:)/gm, '').toUpperCase() + ' === ' + word.replace(/(\,|\;|\.|\!|\?|\'|\’|\:)/gm, '').toUpperCase());
      if (array[current].replace(/(\,|\;|\.|\!|\?|\'|\’|\:)/gm, '').toUpperCase() == word.replace(/(\,|\;|\.|\!|\?|\'|\’|\:)/gm, '').toUpperCase()) {
        current = current >= array.length - 1 ? 0 : current + 1; 
        this.setState((prevState) => {
          const style = prevState.style.map((item, index) => index === current ? 1 : 0)
          return { style }
        })
      }
      else if (array[current + 1].replace(/(\,|\;|\.|\!|\?|\'|\’|\:)/gm, '').toUpperCase() === word.replace(/(\,|\;|\.|\!|\?|\'|\’|\:)/gm, '').toUpperCase()) {
        current = current >= array.length - 2 ? 0 : current + 2; 
        this.setState((prevState) => {
          const style = prevState.style.map((item, index) => index === current ? 1 : 0)
          return { style }
        })
      }
    });  
  }

      render() {
        return (
          <div style= {{
            backgroundColor : 'black'
          }}>
          <button onClick={() => {
            axios.post('http://localhost:8080/process', {text} )
              .then((result) => {
              })
          }}> Submit
          </button>
          <div/>
        {array.map( (word, index) => {
          return (this.state.style[index] === 1 ? (
            <span style={{fontSize: 72, fontWeight: 'bold', color: 'yellow'}}>{word} </span>
          ) : (<span style={{fontSize: 72, fontWeight: 'bold', color: 'white'}}>{word} </span>))
        })}
        </div>
        )
      }
}

export default App;
